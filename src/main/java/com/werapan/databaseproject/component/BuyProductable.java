package com.werapan.databaseproject.component;

import com.werapan.databaseproject.model.Product;

/**
 *
 * @author NutLekKung
 */
public interface BuyProductable {

    public void buy(Product product, int qty);
}
