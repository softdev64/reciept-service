package com.werapan.databaseproject.component;

import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.service.ProductService;
import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author NutLekKung
 */
public class ProductListPanel extends javax.swing.JPanel implements BuyProductable {

    private final ProductService productService;
    private final ArrayList<Product> products;
    private final ArrayList<BuyProductable> subscribers = new ArrayList<>();

    public ProductListPanel() {
        initComponents();
        productService = new ProductService();
        products = productService.getProductsOrderByName();
        int productSize = products.size();
        for (Product p : products) {
            ProductItemPanel pnlProductItem = new ProductItemPanel(p);
            pnlProductItem.addOnBuyProduct(this);
            pnlProductList.add(pnlProductItem);
        }
        pnlProductList.setLayout(new GridLayout((productSize / 3) + ((productSize % 3 != 0) ? 1 : 0), 3, 0, 0));
    }

    public void addOnBuyProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlProductList = new javax.swing.JPanel();

        pnlProductList.setBackground(new java.awt.Color(255, 204, 255));

        javax.swing.GroupLayout pnlProductListLayout = new javax.swing.GroupLayout(pnlProductList);
        pnlProductList.setLayout(pnlProductListLayout);
        pnlProductListLayout.setHorizontalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 507, Short.MAX_VALUE)
        );
        pnlProductListLayout.setVerticalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 509, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlProductList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlProductList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel pnlProductList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        System.out.println("" + product.getName() + " " + qty);
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }
    }
}
