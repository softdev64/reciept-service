package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

public class ProductService {

    private ProductDao productDao = new ProductDao();

    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name ASC ");
    }

//    public Product addNew(Product editedProduct) {
//        ProductDao productDao = new ProductDao();
//        return productDao.save(editedProduct);
//    }
//
//    public Product update(Product editedProduct) {
//        ProductDao productDao = new ProductDao();
//        return productDao.update(editedProduct);
//    }
//
//    public int delete(Product editedProduct) {
//        ProductDao productDao = new ProductDao();
//        return productDao.delete(editedProduct);
//    }
}
