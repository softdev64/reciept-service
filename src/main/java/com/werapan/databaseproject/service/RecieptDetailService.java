package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.RecieptDetailDao;
import com.werapan.databaseproject.model.RecieptDetail;
import java.util.List;

public class RecieptDetailService {

    public List<RecieptDetail> getRecieptDetails() {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getAll(" reciept_detail_id asc");
    }

    public RecieptDetail addNew(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.save(editedRecieptDetail);
    }

    public RecieptDetail update(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.update(editedRecieptDetail);
    }

    public int delete(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.delete(editedRecieptDetail);
    }
}
