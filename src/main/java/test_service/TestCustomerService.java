package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

public class TestCustomerService {

    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0912024202"));

        Customer cus1 = new Customer("Lilia", "0923234242");
        cs.addNew(cus1);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

        Customer delCus = cs.getByTel("0923234242");
        delCus.setTel("0823234242");
        cs.update(delCus);
        System.out.println("After Updated");
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }

        cs.delete(delCus);
        for (Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
